# Recon suggester

Use an existing nmap scan to suggest more recon

## Usage

```
usage: recon-suggest.py [-h] --xml XML

Suggest some recon.

optional arguments:
  -h, --help  show this help message and exit
  --xml XML   input nmap xml
```

# Credits

[AutoRecon](https://github.com/Tib3rius/AutoRecon) - Source of mostly all the nmap and follow up recon commands

