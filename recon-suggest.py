import xmltodict
import json
import argparse

class Suggestion():
    def __init__(self, **kwargs):
        self.port = kwargs.pop('port')
        self.proto = kwargs.pop('proto')

    def build(self, **kwargs):
        raise NotImplemented

class NmapSuggestion(Suggestion):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.scripts = kwargs['scripts']

    def build(self, **kwargs) -> str:
        nmap_extra = ''
        if self.proto == 'udp':
            nmap_extra += ' -sU'
            
        template = 'nmap {nmap_extra} -sV -p {port} --script="{scripts}" -oA "{out_dir}/{proto}_{port}_nmap" {address}'
        kwargs.pop('port')
        kwargs.pop('proto')
        return template.format(port=self.port, proto=self.proto, nmap_extra=nmap_extra, **kwargs)

port_service_map = {
    "21/tcp": "ftp",
    "22/tcp": "ssh",
    "23/tcp": "telnet",
    "123/tcp": "ntp",
    "123/udp": "ntp"
}

nmap_script_map = {
    "mongodb": [
        "banner,(mongodb* or ssl*) and not (brute or broadcast or dos or external or fuzzer)"
    ],
    "ms-sql": [
        "banner,(ms-sql* or ssl*) and not (brute or broadcast or dos or external or fuzzer)"
    ],
    "mysql": [
        "banner,(mysql* or ssl*) and not (brute or broadcast or dos or external or fuzzer)"
    ],
    "oracle": [
        "banner,oracle-sid-brute",
        "banner,(oracle* or ssl*) and not (brute or broadcast or dos or external or fuzzer)"
    ],
    "dns": [
        "banner,(dns* or ssl*) and not (brute or broadcast or dos or external or fuzzer)"
    ],
    "ftp": [
        "banner,(ftp* or ssl*) and not (brute or broadcast or dos or external or fuzzer)"
    ],
    "http": [
        "banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)"
    ],
    "krb": [
        "banner,krb5-enum-users"
    ],
    "ldap": [
        "banner,(ldap* or ssl*) and not (brute or broadcast or dos or external or fuzzer)"
    ],
    "cassandra": [
        "banner,(cassandra* or ssl*) and not (brute or broadcast or dos or external or fuzzer)"
    ],
    "cups": [
        "banner,(cups* or ssl*) and not (brute or broadcast or dos or external or fuzzer)"
    ],
    "distcc": [
        "banner,distcc-cve2004-2687"
    ],
    "finger": [
        "banner,finger"
    ],
    "imap": [
        "banner,(imap* or ssl*) and not (brute or broadcast or dos or external or fuzzer)"
    ],
    "nntp": [
        "banner,nntp-ntlm-info"
    ],
    "ntp": [
        "banner,(ntp* or ssl*) and not (brute or broadcast or dos or external or fuzzer)"
    ],
    "pop3": [
        "banner,(pop3* or ssl*) and not (brute or broadcast or dos or external or fuzzer)"
    ],
    "rmi": [
        "banner,rmi-vuln-classloader,rmi-dumpregistry"
    ],
    "telnet": [
        "banner,telnet-encryption,telnet-ntlm-info"
    ],
    "tftp": [
        "banner,tftp-enum"
    ],
    "vnc": [
        "banner,(vnc* or realvnc* or ssl*) and not (brute or broadcast or dos or external or fuzzer)"
    ],
    "nfs": [
        "banner,(rpcinfo or nfs*) and not (brute or broadcast or dos or external or fuzzer)",
        "banner,nfs* and not (brute or broadcast or dos or external or fuzzer)"
    ],
    "rdp": [
        "banner,(rdp* or ssl*) and not (brute or broadcast or dos or external or fuzzer)"
    ],
    "redis": [
        "banner,redis-info"
    ],
    "rpc": [
        "banner,msrpc-enum,rpc-grind,rpcinfo"
    ],
    "rsync": [
        "banner,(rsync* or ssl*) and not (brute or broadcast or dos or external or fuzzer)"
    ],
    "sip": [
        "banner,sip-enum-users,sip-methods"
    ],
    "smb": [
        "banner,(nbstat or smb* or ssl*) and not (brute or broadcast or dos or external or fuzzer)",
        "smb-vuln-ms06-025",
        "smb-vuln-ms07-029",
        "smb-vuln-ms08-067",
        "smb-vuln-ms06-025",
        "smb-vuln-ms07-029",
        "smb-vuln-ms08-067"
    ],
    "smtp": [
        "banner,(smtp* or ssl*) and not (brute or broadcast or dos or external or fuzzer)"
    ],
    "snmp": [
        "banner,(snmp* or ssl*) and not (brute or broadcast or dos or external or fuzzer)"
    ],
    "ssh": [
        "banner,ssh2-enum-algos,ssh-hostkey,ssh-auth-methods"
    ]
}


parser = argparse.ArgumentParser(description='Suggest some recon.')
parser.add_argument('--xml', action='store', required=True,
                    help='input nmap xml')

args = parser.parse_args()

with open(args.xml) as f:
    xml_content = f.read()
    nmap_dict = xmltodict.parse(xml_content)
    f.close()

#print(json.dumps(nmap_dict, indent=4, sort_keys=True))
for host in nmap_dict['nmaprun'].get('host', []):
    print(host['address']['@addr'])
    ports = host['ports'].get('port', [])
    if not isinstance(ports, list):
        ports = [ports]
    for port in ports:
        num = port['@portid']
        proto = port['@protocol']
        state = port['state']['@state']
        print(f'{num}/{proto} {state}')
        if state == 'open':
            for script_set in nmap_script_map[port_service_map[f'{num}/{proto}']]:
                options = {
                    "port": num,
                    "proto": proto,
                    "scripts": script_set,
                    "address": host['address']['@addr'],
                    "out_dir": '/tmp'
                }
                suggestion = NmapSuggestion(**options)
                cmd = suggestion.build(**options)
                print(cmd)


for num, proto in [("123", "udp"), ("23", "tcp"), ("21", "tcp"), ("123", "tcp")]:
    for script_set in nmap_script_map[port_service_map[f'{num}/{proto}']]:
        options = {
            "port": num,
            "proto": proto,
            "scripts": script_set,
            "address": host['address']['@addr'],
            "out_dir": '/tmp'
        }
        suggestion = NmapSuggestion(**options)
        cmd = suggestion.build(**options)
        print(cmd)

